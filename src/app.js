import 'fetch';
import {inject, LogManager} from 'aurelia-framework';
import {Router} from 'aurelia-router';
import {HttpClient} from 'aurelia-fetch-client';
import AuthorizeStep from './config/authorize-step';
import AuthService from './services/auth';
import {CarroService} from './services/carroService';
import {Config} from './config/config.js';
import toastr from 'toastr';
@inject(Router, HttpClient, AuthService, CarroService, Config)
export class App {

    logger = LogManager.getLogger("name");

    constructor(router, http, auth, carroService, config) {
        this.auth = auth;
        this.config = config;
        toastr.options.closeButton = true;
        console.log(toastr.options.closeButton);
        http.configure(config => {
            config
                .withBaseUrl(this.config.urlApi)
                .withDefaults({
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .withInterceptor({
                    request(request) {
                        if (auth.loggedIn) {
                            request.headers.append('Authorization', `Bearer ${auth.getToken()}`);
                        }
                        return request;
                    },
                    response(response) {
                        console.error(response);
                        if (response.status === 401) {
                            auth.removeToken();
                            router.navigateToRoute('login');
                        }
                        return response;
                    }
                });
        });
        this.carroService = carroService;
    }

    get user(){
        return this.auth.user;
    }

    configureRouter(config, router) {
        config.title = 'Buffet IAE';
        config.addPipelineStep('authorize', AuthorizeStep);
        config.map([
            {route: 'login', name: 'login', moduleId: './views/auth/login', title: 'Login', auth: false},
            {route: 'menu', name: 'menu', moduleId: './views/menu', nav: true, title: 'Inicio', auth: true},
            {route: 'solo-carrito', name: 'carrito', moduleId: 'solo-carrito', nav: true, title: 'Carrito', auth: true},
            {route: 'print', name: 'print', moduleId: './components/print', nav: false, title: 'Imprimir', auth: true},
            {route: '', redirect: 'menu'}
        ]);

        this.router = router;
    }
}
