//import {HttpClient} from 'aurelia-fetch-client';
import {inject} from 'aurelia-framework';
import {computedFrom} from 'aurelia-framework';
import $ from 'jquery';
import {Config} from '../config/config.js';
import AuthService from '../services/auth';
import toastr from 'toastr';
import {Router} from 'aurelia-router';
@inject(Config, AuthService, Router)
export class CarroService {
    articulos = [];
    total = 0;

    constructor(config, auth, router) {
        this.config = config;
        this.articulos = [];
        this.url = `${this.config.urlApi}/buffet_cierra`;
        this.auth = auth;
        this.router = router;
    }

    add(articulo) {
        this.articulos.push(articulo);
        this.total += Number(articulo.precio_venta);
        toastr.info(`${articulo.nombre} fué añadido`, 'Carrito');
    }

    remove(articulo) {
        var i = this.articulos.indexOf(articulo);
        this.articulos.splice(i, 1);
        this.total -= Number(articulo.precio_venta);
        //this.articulos.pop(articulo);
    }

    @computedFrom('this.articulos.length')
    get carroConArticulos() {
        console.log("carroConArticulos", this.articulos.length);
        return this.articulos.length !== 0;
    }

    cierra() {
        $.ajax({
                url: this.url,
                type: 'POST',
                dataType: 'json',
                crossDomain: true,
                data:{
                    doc:{socio_id:this.auth.user.socio_id, total:this.total},
                    items:this.articulos
                },
                beforeSend: (request) =>
                {
                    //request.setRequestHeader("Authorization", this.auth.getToken());
                },
                crossDomain: true,
                error: (data) => {
                    console.log('error',data);
                }
            })
            .then(response=> {
                console.log('cierra',response.data,response);
                if (response.status === 'ok') {
                    this.vaciaCarrito();
                    toastr.info(`Su pedido fue grabado y está siendo impreso`, 'Cierre');
                    setTimeout(()=>{

                    },2000);
                    this.imprime();
                    return response;
                }
                //throw new Error(response.statusText);
            });
        //$.getJSON(`${this.url}`, {data: JSON.stringify(this.articulos)},data=>{
        //        if(data.status === 'ok'){
        //            alert('Ok');
        //        }
        //    });

        return;
    }

    imprime(){
        this.router.navigate('#/print');
    }

    vaciaCarrito(){
        this.articulos = [];
    }
}