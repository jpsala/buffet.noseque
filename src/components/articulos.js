import {bindable, containerless, inject, computedFrom} from 'aurelia-framework';
import {DialogService} from 'aurelia-dialog';
import {DlgSub} from './../views/dlgSub';
import {Menu} from './../views/menu';
@inject(DialogService, Menu)
export class Articulos {
    @bindable articulos = undefined;
    carro = undefined;
    dlg = undefined;
    articulo;
    menu = undefined;

    constructor(dlg, menu) {
        this.dlg = dlg;
        this.menu = menu;
    }

    @computedFrom(["articulos[]"])
    get articulosActivos() {
        return this.articulos.filter(x => {
                return x.borrado == 0;
            }
        )
    }

    addArticuloToCarrito(articulo) {
        if (articulo.subs.length > 0) {
            this.dlg.open({viewModel: DlgSub, model: articulo}).then(response => {
                if (!response.wasCancelled) {
                    let subArticulo = response.output;
                    let articuloNuevo = {
                        nombre: articulo.nombre,
                        subNombre: subArticulo.nombre,
                        subId: subArticulo.id,
                        id: articulo.id,
                        precio_venta: subArticulo.precio_venta
                    };
                    this.menu.addArticuloToCarrito(articuloNuevo);
                }
            })
        } else {
            this.menu.addArticuloToCarrito(articulo);
        }
    }

}