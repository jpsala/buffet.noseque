import {bindable, containerless} from 'aurelia-framework';
import {inject} from 'aurelia-framework';
import {Menu} from '../views/menu.js';
//@containerless()
@inject(Menu)
export class ArticuloCarrito {
    menu;
    @bindable articulo = undefined;
    constructor(menu){
        this.menu = menu;
    }

    attached() {
        //this.articulo.imagenPath = `./images/${this.articulo.imagen || this.articulo.nombre + '.jpg'}`;
    }

    remove(articulo){
        this.menu.removeArticuloFromCarrito(articulo);
    }
}