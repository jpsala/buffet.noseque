import {bindable, containerless} from 'aurelia-framework';
//@containerless()
export class ArticuloMenu{
    @bindable articulo = null;
    attached() {
        //this.articulo.imagenPath = `/buffet/images/${this.articulo.imagen || this.articulo.nombre + '.jpg'}`;
        this.articulo.imagenPath = `./images/${this.articulo.imagen.trim()}`;
        //console.log(this.articulo.imagenPath);
    }

}