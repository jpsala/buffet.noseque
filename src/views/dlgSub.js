import {DialogController} from 'aurelia-dialog';

export class DlgSub {
    static inject = [DialogController];
    articulo = undefined;

    constructor(controller) {
        this.controller = controller;
        this.controller.settings.lock = false;
    }

    activate(articulo) {
        this.articulo = articulo;
    }
}