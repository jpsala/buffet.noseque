import {inject, computedFrom} from 'aurelia-framework';
import {CarroService} from './../services/carroService';
import {ArticulosService} from './../services/articulosService';
import {Router} from 'aurelia-router';
import $ from 'jquery';
@inject(CarroService, ArticulosService)
export class Menu {
    articulosService;
    articulos = undefined;
    articulosCarro = [];
    constructor(carroService, articulosService) {
        this.carroService = carroService;
        this.articulosService = articulosService;
    }

    activate() {
        return this.articulosService.getArticulos()
            .then(r=>this.articulos = r)
            .then(()=>{
                this.articulosCarro = this.articulos;
            })
    }

    attached() {
        this.calcHeight();
        this.scrollMenu();
        $(window).resize(()=>{
            this.calcHeight();
            this.scrollMenu();
        });

    }

    addArticuloToCarrito(a) {
        this.carroService.add(a);
        this.calcHeight();
        this.scrollMenu();
    }

    removeArticuloFromCarrito(articulo) {
        this.carroService.remove(articulo);
        //this.articulos.push(articulo);
        this.calcHeight();
        this.scrollMenu();
    }

    calcHeight() {
        let height = $(window).height() - $('.navbar').height() - $('footer').height() - 25;
        $('.menu').height(height);
        $('.carrito').height(height);
    };

    scrollMenu(){
        var d = $('.carrito');
        setTimeout(()=>d.scrollTop(d.prop("scrollHeight")),0);
    }
}